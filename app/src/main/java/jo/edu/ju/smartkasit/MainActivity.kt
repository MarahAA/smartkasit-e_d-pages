package jo.edu.ju.smartkasit

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mDining.setOnClickListener{
            var intent = Intent(this, DiningListActivity::class.java)
            startActivity(intent)
        }

        mEvents.setOnClickListener{
            var intent = Intent(this, EventListActivity::class.java)
            startActivity(intent)
        }
    }
}
