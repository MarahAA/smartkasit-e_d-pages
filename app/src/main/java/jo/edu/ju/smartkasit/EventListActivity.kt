package jo.edu.ju.smartkasit

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_event_list.*

class EventListActivity : AppCompatActivity() {

    var list = mutableListOf<Model>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_list)



        list.add(Model("Flower" , "Flowers are plants. wacky, i know.", R.drawable.ic_event_note_black_24dp))
        list.add(Model("Sun Flares" , "They're hot. Proven Scientifically", R.drawable.ic_event_note_black_24dp))
        list.add(Model("Free Breakfast" , "only for 9.99$", R.drawable.ic_event_note_black_24dp))
        list.add(Model("Stars" , "Stars are Lit. *-*-* ", R.drawable.ic_event_note_black_24dp))
        list.add(Model("Hot Tubs" , "Kill people. Read more.", .ic_event_note_black_24dp))
        list.add(Model("Smile" , " 'Uhm, you should smile more' - famous last words", .ic_event_note_black_24dp))

        mListView.adapter = MyListAdapter(this, R.layout.raw_list_view, list)

        mListView.setOnItemClickListener {parent, view, position, id ->
            if(position == 0){
                goToDineAct(0)
            }

            if(position == 1){
                goToDineAct(1)
            }

            if(position == 2){
                goToDineAct(2)
            }

            if(position == 3){
                goToDineAct(3)
            }

            if(position == 4){
                goToDineAct(0)
            }

            if(position == 5){
                goToDineAct(5)
            }

        }


    }

    fun goToDineAct(position : Int) {
        var title : String = list[position].title
        var desc : String = list[position].description
        var img : Int = list[position].photo
        var intent = Intent(applicationContext, EventActivity::class.java)
        intent.putExtra("Title", title)
        intent.putExtra("Desc", desc)
        intent.putExtra("Image", img)
        startActivity(intent)
    }

}


